# Script to crossbuild on Windows because fucking powershell can't do:
# GOOS=linux GOARCH=amd64 go build
# enhanced with running built app via wsl if built for linux

param (
    [Parameter(Mandatory, Position = 0)][System.IO.DirectoryInfo]$Path,
    [string]$Os = "windows",
    [string]$Arch = "amd64"
)

# PATH TO PROJECT INSIDE WSL
# CHANGE ME TO YOUR PATH
$WslPath = "/mnt/e/code/golang/src/gitlab.com/stefanjarina/udemy_learngo/bin"

# set env variables (these are set globaly for current powershell session, thus stay changed even
# when script executes, therefore we will need to change them back to defaults later)
$Env:GOOS = $Os;
$Env:GOARCH = $Arch;

# build go source and output binary to bin folder
go build -o $PSScriptRoot/bin/ $Path;

# run program either in WSL if built for linux or in powershell when built for windows
if ($Os -like "linux") {
    wsl $WslPath/$($Path.Name)
}
 else {
    & "$PSScriptRoot/bin/$($Path.Name).exe"
}

# reset variables back to defaults for windows
$Env:GOOS = "windows";
$Env:GOARCH = "amd64";

